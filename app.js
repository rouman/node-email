console.log('* [example1] sending test email');
 
// Require the module and set default options 
// You may use almost any option available in nodemailer,  
// but if you need fine tuning I'd recommend to consider using nodemailer directly. 
var send = require('gmail-send')({
  user: 'ivanrouman@gmail.com',               // Your GMail account used to send emails 
  pass: 'qaz80qaz80qaz',             // Application-specific password 
  to:   'ivanrouman@gmail.com>',      // Send back to yourself 
  subject: 'test subject',
  text:    'test text',
  html:    '<b>html text text</b>' 
});

// Override any default option and send email 
send({                         
  subject: 'attached ',   // Override value set as default  
}, function (err, res) {
  console.log('* [example1] send(): err:', err);
});